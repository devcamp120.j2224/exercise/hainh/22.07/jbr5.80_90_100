package com.example.demo.Service;
import java.util.ArrayList;
import org.springframework.stereotype.Service;
import com.example.demo.Model.Artist;
import com.example.demo.Model.Band;
import com.example.demo.Model.BandMember;
import com.example.demo.Model.Composer;

@Service
public class ComposerService {

        // khởi tạo đối tượng  Artist
        Artist DanTruong = new Artist("Đan", "Trường", "bo");
        Artist PhuongThanh = new Artist("Phương", "Thanh", "chanh");
        Artist MyTam = new Artist("Mỹ", "Tâm", "bé");


        // khởi tạo đối tượng bandMember 
        BandMember bMem1 = new BandMember("band 1", "member 1", "stage name 1", "pop");
        BandMember bMem2 = new BandMember("band 2", "member 2", "stage name 2", "slow rock");
        BandMember bMem3 = new BandMember("band 3", "member 3", "stage name 3", "rock");
        BandMember bMem4 = new BandMember("band 4", "member 4", "stage name 4", "hip hop");
        BandMember bMem5 = new BandMember("band 5", "member 5", "stage name 5", "rock alternative");
        BandMember bMem6 = new BandMember("band 6", "member 6", "stage name 6", "disco");
        
    // tạo 1 arraylist artist gồm 3 artist
    public ArrayList<Artist> getArtists(){

        DanTruong.setAlbums(AlbumService.getAlbumDanTruong());
        PhuongThanh.setAlbums(AlbumService.getAlbumPhuongThanh());
        MyTam.setAlbums(AlbumService.getAlbumMyTam());

        ArrayList<Artist> artists = new ArrayList<>();

        artists.add(DanTruong);
        artists.add(PhuongThanh);
        artists.add(MyTam);

        return artists ;
    }
    // tạo 1 arraylist artist gồm 3 artist 6 band members 
    public ArrayList<Composer> getComposeres(){
        
        DanTruong.setAlbums(AlbumService.getAlbumDanTruong());
        PhuongThanh.setAlbums(AlbumService.getAlbumPhuongThanh());
        MyTam.setAlbums(AlbumService.getAlbumMyTam());

        ArrayList<Composer> composeres = new ArrayList<>();

        composeres.add(DanTruong);
        composeres.add(PhuongThanh);
        composeres.add(MyTam);

        composeres.add(bMem1);
        composeres.add(bMem2);
        composeres.add(bMem3);
        composeres.add(bMem4);
        composeres.add(bMem5);
        composeres.add(bMem6);

        return composeres ;
    }  

    // tạo 3 band nhạc add 6 bandmemmber ở trên zô mỗi band là 1 arraylist
    public ArrayList<Band> getBands(){

        ArrayList<BandMember> bMember1 = new ArrayList<>();

        bMember1.add(bMem1);
        bMember1.add(bMem2);
       

        ArrayList<BandMember> bMember2 = new ArrayList<>();

        bMember2.add(bMem4);
        bMember2.add(bMem5);
      

        ArrayList<BandMember> bMember3 = new ArrayList<>();

        bMember3.add(bMem5);
        bMember3.add(bMem6);

        // khởi tạo đối tượng Band và nhét dữ liệu là arraylist bmember vừa tạo ở trên zô
        Band band1 = new Band("chó ăn cức",bMember1, AlbumService.getAlbumDanTruong());
        Band band2 = new Band("chó đẻ",bMember2, AlbumService.getAlbumPhuongThanh());
        Band band3 = new Band("chó ngục",bMember3, AlbumService.getAlbumMyTam());
    

        // tạo 1 arraylist tổng add 3 band vào
        ArrayList<Band> bands = new ArrayList<>();

        bands.add(band1);
        bands.add(band2);
        bands.add(band3);

        return bands ;
    }

}
