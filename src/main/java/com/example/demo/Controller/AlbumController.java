package com.example.demo.Controller;
import java.util.ArrayList;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.Model.Album;
import com.example.demo.Service.AlbumService;

@RestController
public class AlbumController {
    
        @GetMapping("/albums")
        public ArrayList<Album> getAlllbums(){

            ArrayList<Album> albums = AlbumService.getAlbumList();

            return albums ;
        }
    }
