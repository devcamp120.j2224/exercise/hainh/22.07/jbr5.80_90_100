package com.example.demo.Controller;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.Model.Artist;
import com.example.demo.Model.Band;
import com.example.demo.Model.Composer;
import com.example.demo.Service.ComposerService;

@RestController
public class ComposerController {
  
    @Autowired
    ComposerService composerService;
    /* tại sao lại autowired vào lúc này , vì bên ComposerService đã tạo các biến toàn cục 

    như Artist vs band member , nên trường hợp này chỉ định xài autowired để không phải khai 

    báo lại từ đầu trong các methods 

    getBands , getArtists , getComposeres

     */ 
    @GetMapping("/bands")
    public ArrayList<Band> getAllBand(){
        ArrayList<Band> allBand = composerService.getBands();
        return allBand ;
    }

    @GetMapping("/artists")
    public ArrayList<Artist> getAllArtist(){
        ArrayList<Artist> all = composerService.getArtists();
        return all ;
    }

    @GetMapping("/composeres")
    public ArrayList<Composer> getAllComposer(){
        ArrayList<Composer> all = composerService.getComposeres();
        return all ;
    }
}
